import Brand from "../components/Brand";
import Social from "../components/Social";

const Pricing = () => {
  return(
    <>
      <div className="col-12 pricing--wrapper">
        <Social />
        <div className="col-12 pricing--wrap">
          <div className="container-custom">
            <div className="col-12 pricing--subwrap">
              <div className="col-12 col-lg-6 header-wrap">
                <h4 className="head-text">Fiyatlandırma</h4>
                <p className="desc-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
              </div>
              <div className="col-12 col-lg-11 pricing--item-wrap">
                <div className="col-12 col-md-5 col-lg-4 pricing-item">
                  <div className="item-header blue">
                    Standart Plan
                  </div>
                  <ul className="item-content">
                    <li className="price">
                      <span>20₺</span> + KDV
                    </li>
                    <li className="free">
                      1 Yıl Boyunca Ücretsiz
                    </li>
                    <div className="dot-slice"/>
                    <li className="info-text">
                      Aylık Ödeme ile istediğiniz
                      zaman bırakabilirsiniz.
                    </li>
                    <li className="info-text">
                      3 kullanıcı oluşturma hakkı
                    </li>
                    <li className="info-text">
                      Ekstra 2 fatura şablonu
                    </li>
                    <li className="info-text">
                      350 Cari/Ürün, 1000 Fatura
                      (Aylık - E-Fatura Dahil Değildir.)
                    </li>
                    <li>
                      <div className="btn-primary">
                       Satın Al
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="col-12 col-md-5 col-lg-4 pricing-item bigger">
                  <div className="item-header orange">
                    Popüler Plan
                  </div>
                  <ul className="item-content">
                    <li className="price">
                      <span>40₺</span> + KDV
                    </li>
                    <li className="free">
                      1 Yıl Boyunca Ücretsiz
                    </li>
                    <div className="dot-slice"/>
                    <li className="info-text">
                      Aylık Ödeme ile istediğiniz
                      zaman bırakabilirsiniz.
                    </li>
                    <li className="info-text">
                      3 kullanıcı oluşturma hakkı
                    </li>
                    <li className="info-text">
                      Ekstra 2 fatura şablonu
                    </li>
                    <li className="info-text">
                      350 Cari/Ürün, 1000 Fatura
                      (Aylık - E-Fatura Dahil Değildir.)
                    </li>
                    <li>
                      <div className="btn-secondary">
                        Satın Al
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="col-12 col-md-5 col-lg-4 pricing-item">
                  <div className="item-header purple">
                    Premium Plan
                  </div>
                  <ul className="item-content">
                    <li className="price">
                      <span>80₺</span> + KDV
                    </li>
                    <li className="free">
                      1 Yıl Boyunca Ücretsiz
                    </li>
                    <div className="dot-slice"/>
                    <li className="info-text">
                      Aylık Ödeme ile istediğiniz
                      zaman bırakabilirsiniz.
                    </li>
                    <li className="info-text">
                      3 kullanıcı oluşturma hakkı
                    </li>
                    <li className="info-text">
                      Ekstra 2 fatura şablonu
                    </li>
                    <li className="info-text">
                      350 Cari/Ürün, 1000 Fatura
                      (Aylık - E-Fatura Dahil Değildir.)
                    </li>
                    <li>
                      <div className="btn-primary">
                        Satın Al
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Brand />
    </>
  )
}

export default Pricing