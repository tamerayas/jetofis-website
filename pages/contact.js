import PageHeader from "../components/PageHeader";
import Brand from "../components/Brand";
import {useEffect, useState} from "react";

const Contact = () => {
  let [contact,setContact] = useState({name:"",surname:"",mail:"",messages:""})

  const handleContactInputs = (e) =>{
    setContact({...contact,[e.target.name] : e.target.value})

  }

  const contactSubmit = async (e) => {
    console.log(contact);
    const contactResponse = await fetch("http://localhost:8888/api/contact",{
      method:'POST',
      headers:{
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify(contact)
    })

    const content = await contactResponse.json()

  }

  return(
    <>
      <PageHeader head="İletişim" desc="Tavsiyelerinizi / Önerilerinizi veya Sorularınızı bekliyoruz."/>

      <div className="col-12 maps--wrapper">
        <iframe id="gmap_canvas"
                src="https://maps.google.com/maps?q=Kahramanmara%C5%9F%20KSU%20%C3%9Cniversitesi,%20Av%C5%9Far%20Kamp%C3%BCs%C3%BC%20Teknokent%20Bina%20No:%20259%20Kat%202%20No:%2041%20ON%C4%B0K%C4%B0%C5%9EUBAT%20KAHRAMANMARA%C5%9E/T%C3%9CRK%C4%B0YE&t=&z=13&ie=UTF8&iwloc=&output=embed"
                frameBorder="0" scrolling="no"></iframe>
      </div>


      <div className="col-12 contact--wrapper">
        <div className="container-custom">
          <div className="contact--wrap">
            <div className="col-12 col-lg-6 contact--form-wrapper">
              <div className="input-label-wrap w50">
                <label><i className="form-user"></i> Adınız</label>
                <div className="col-12 input-control">
                  <input key="name" name="name" onChange={handleContactInputs} value={contact.name} type="text" placeholder="Adınzı Yazınız..."/>
                </div>
              </div>
              <div className="input-label-wrap w50">
                <label>Soyadınız</label>
                <div className="col-12 input-control">
                  <input key="surname" name="surname" onChange={handleContactInputs} value={contact.surname} type="text" placeholder="Adınzı Yazınız..."/>
                </div>
              </div>
              <div className="input-label-wrap">
                <label><i className="form-email"></i>E-Mail</label>
                <div className="col-12 input-control">
                  <input key="mail" name="mail" onChange={handleContactInputs} type="email" value={contact.mail} placeholder="Adınzı Yazınız..."/>
                </div>
              </div>
              <div className="input-label-wrap">
                <label><i className="form-message"></i>Mesajınız</label>
                <div className="col-12 textarea-control">
                  <textarea key="messages" name="messages" onChange={handleContactInputs} value={contact.messages} placeholder="Adınzı Yazınız..."/>
                </div>
              </div>
              <div className="col-12 button-wrapper">
                <button className="col-12 btn-primary" onClick={contactSubmit}>Gönder</button>
              </div>
            </div>
            <div className="col-12 col-lg-6 contact--info-wrapper">
              <div className="col-12 info-item">
                <h4 className="head-text">Tsoft Yerel Yönetim Merkezi</h4>
                <p className="head-desc">Esentepe Mahallesi, Haberler Sk. No: 13 ŞİŞLİ / İSTANBUL / TÜRKİYE</p>
                <a className="info-tel-wrap" href="tel:902124800048"> <i className="info-tel"></i> Telefon: +90 212 480 00 48</a>
              </div>
              <div className="col-12 info-item">
                <h4 className="head-text">İstanbul Ar-Ge Ofisi</h4>
                <p className="head-desc">Yıldız Teknik Üniversitesi, Davutpaşa Kampüsü Teknopark D2 Blok KAT: 2 No: 104 ESENLER / İSTANBUL / TÜRKİYE</p>
                <a className="info-tel-wrap" href="tel:902124800048"> <i className="info-tel"></i> Telefon: +90 212 483 71 64</a>
              </div>
              <div className="col-12 info-item">
                <h4 className="head-text">Anadolu Ar-Ge Ofisi</h4>
                <p className="head-desc">Kahramanmaraş KSU Üniversitesi, Avşar Kampüsü Teknokent Bina No: 259 Kat 2 No: 41 ONİKİŞUBAT KAHRAMANMARAŞ/TÜRKİYE</p>
                <a className="info-tel-wrap" href="tel:902124800048"> <i className="info-tel"></i> Telefon: +90 344 502 03 25</a>
              </div>
              <div className="col-12 info-item">
                <h4 className="head-text">T-Soft Inc. Global Genel Merkezi</h4>
                <p className="head-desc">906 Broadway, Suite 100, San Francisco, CA, 94133 USA</p>
                <a className="info-tel-wrap" href="tel:902124800048"> <i className="info-tel"></i> Telefon: +1 408 220 64 68</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Brand />
    </>
  )

}

export default Contact