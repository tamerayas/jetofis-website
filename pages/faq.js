import Faq from "../components/Faq";
import Brand from "../components/Brand";
import BreadCrumb from "../components/BreadCrumb";
import PageHeader from "../components/PageHeader";


const faq = () =>{

  return(
    <>
      <PageHeader head="Sıkça Sorulan Sorular" desc="Hızlıca destek almak için derlediğimiz içeriklere göz atın."/>
      <BreadCrumb breadcrumb={[{name:"Destek",href: "/support"},{name:"Sıkça Sorulan Sorular",href:"/faq"}]}/>
      <Faq header={false}/>
      <Brand />
    </>
  )

}

export default faq
