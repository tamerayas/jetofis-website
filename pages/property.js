import Social from "../components/Social";
import Image from "next/image";
import BreadCrumb from "../components/BreadCrumb";
import DescriptionBanner from "../components/DescriptionBanner";
import propertyState from "../data/property"
import {useState} from "react";
import Brand from "../components/Brand";


const Property = () => {
  let [property,setProperty] = useState(propertyState)
  return(
    <>
      <div className="showcase-header-wrapper">
        <Social />
        <div className="col-12 showcase-header-wrap">
          <div className="container-custom">
            <div className="col-5 content-wrap">
              <h4 className="head-text">
                Özellikler
              </h4>
              <p className="desc-text">
                Hızlı, kolay ve ücretsiz faturalayın!
              </p>
              <p className="sub-desc-text">
                E-Fatura, E-İrsaliye, Gelen E-Fatura, Pazaryeri Çözümü, Online Tahsilat, E-Ticaret Entegrasyonu
              </p>
            </div>
            <div className="img-wrap">
              <Image src="/images/property-head.webp" width={397} height={350}></Image>
            </div>
          </div>
        </div>
      </div>
      <BreadCrumb breadcrumb={[{name:"Uygulamalar",href:"/app"}]}/>
      {
        property.map((p,key) => (
          <DescriptionBanner head={p.header} desc={p.desc} link={p.link} src={p.image} key={key}/>
        ))
      }
      <Brand />

    </>
  )
}
export default Property