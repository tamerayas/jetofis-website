import Link from "next/link";
import Image from "next/image";
import style from "/styles/modules/Status.module.scss"

const custom404 = () => {
  return(
    <>
      <div className={style.wrap}>
        <div className="container-custom">
          <div className="col-12 col-lg-6 content-wrap">
            <h4 className={style.head}>
              500 Sunucuya Erişilemiyor
            </h4>
            <p className={style.desc}>
              Aşırı Yoğunluktan dolayı sunucuya erişilemiyoru lütfen daha sonra tekrar deneyiniz.
            </p>
            <Link href="/">
              <a className="col-5 btn-primary">
                Anasayfaya Dön
              </a>
            </Link>
          </div>
          <div className="col-12 col-lg-6 img-wrap">
            <Image src="/images/property-head.webp" width={397} height={350}></Image>
          </div>
        </div>
      </div>
    </>
  )
}
export default custom404