import Link from "next/link";
import Image from "next/image";
import style from "/styles/modules/Status.module.scss"

const custom404 = () => {
  return(
    <>
      <div className={style.wrap}>
        <div className="container-custom">
          <div className="col-12 col-lg-6 content-wrap">
            <h4 className={style.head}>
              404 Sayfa Bulunamadı
            </h4>
            <p className={style.desc}>
              Üzgünüz Aradığınız Sayfayı Bulamadık , Tekrar aramayı deneyebilir veya anasayfaya gidebilirsiniz.
            </p>
            <Link href="/">
              <a className="col-5 btn-primary">
                Anasayfaya Dön
              </a>
            </Link>
          </div>
          <div className="col-12 col-lg-6 img-wrap">
            <Image src="/images/property/marketplace.webp" width={470} height={465}></Image>
          </div>
        </div>
      </div>
    </>
  )
}
export default custom404