// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const fs = require('fs');

export default function handler(req, res) {
  const fsReadPromise =  new Promise((resolve,reject) => {

    fs.readFile('./pages/api/data/contact.json', 'utf8', (err, data) => {
      // You should always specify the content type header,
      // when you don't use 'res.json' for sending JSON.
      if(err){
        return reject('error on read')
      }

      resolve(data)
    })


  })
  const {method} = req
  if (method === "GET"){

    return fsReadPromise.then(data =>{
      // res.set('Content-Type', 'application/json');
      return res.json(data)
    })

  }
  if(method === "POST"){
    const {body} = req
    const writeFilePromise = (newData) => new Promise((resolve,reject) => {
      fs.writeFile('./pages/api/data/contact.json', JSON.stringify(newData), async (err) => {
        if (err) {
          return reject('error on write')
        }
        resolve(await fsReadPromise)
      })
    })
    return fsReadPromise.then(oldData => {
      const newData = JSON.parse(oldData)
      newData.push(body)
      return writeFilePromise(newData).then(readPromise => res.json(readPromise))
    })
  }
}
