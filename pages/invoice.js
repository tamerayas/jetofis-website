import PageHeader from "../components/PageHeader";
import BreadCrumb from "../components/BreadCrumb";
import Faq from "../components/Faq";
import Brand from "../components/Brand";
import credits from "../data/credits";
import ProductPropertyCard from "../components/ProductPropertyCard";
import DescriptionBanner from "../components/DescriptionBanner";

const Invoice = () => {

  return(
    <>
      <PageHeader head="E-Fatura" desc="Hızlı,kolay ve ücretsiz faturalayın!"/>
      <BreadCrumb breadcrumb={[{name:"Uygulamalar",href:"/app"},{name:"E-Fatura",href:"/invoice"}]}/>


      <div className="col-12 fast--wrapper">
        <div className="container-custom">
          <div className="fast--wrap">
            <i className="jetofis-single-icon"></i>
            <h4 className="col-12 invoice-header">eFaturayı Kullanmaya Nasıl Başlarım?</h4>
            <p className="head-desc">JetOfis ile cari hesaplarınızı, faturalarınızı, banka ve nakit hesaplarınızı takip edebilirsiniz.
              Birçok özelliği ile işiniz daha da kolaylaşacaktır.</p>
            <DescriptionBanner desc="e-Fatura, kağıt faturanın elektronik ortamda oluşturulmuş halidir. Kağıt fatura ile bire bir aynı hukuki geçerliliğe sahip olan e-fatura, dijitalleşen dünyada firmaların işlerini kolaylaştırır ve hizmet kalitesini artırır. Faturaların güvenli ve hatasız dolaşımını sağlayan e-fatura sistemi, geleneksel faturaya göre çok daha hızlı, ekonomik ve pratiktir."
            desc2="397 Sıra No'lu Vergi Usul Kanunu Genel Tebliğin yayımı ile birlikte Elektronik Fatura Uygulaması (e-Fatura Uygulaması) hizmete alınmış bulunmaktadır. E-Fatura çözümümüz sayesinde, faturalarınızı tek tıkla oluşturabilir, müşterilerinize e-mail yoluyla anında gönderebilirsiniz. Tüm faturalarınızı e-Arşiv fatura platformunda saklayabilir, eski faturalarınıza saniyeler içerisinde ulaşabilirsiniz."
            src="/images/property/invoice.webp"/>
          </div>
        </div>
      </div>
      <div className="col-12 advantage--wrapper">
        <div className="container-custom">
          <div className="advantage--wrap">
            <i className="jetofis-single-icon"></i>
            <h4 className="col-12 invoice-header white">eFaturayı Kullanmaya Nasıl Başlarım?</h4>
            <p className="head-desc">JetOfis ile cari hesaplarınızı, faturalarınızı, banka ve nakit hesaplarınızı takip edebilirsiniz.
              Birçok özelliği ile işiniz daha da kolaylaşacaktır.</p>
            <div className="advantage--item-wrapper">
              <div className="advantage-item">
                <i className="pie-icon"></i>
                <h5>Daha Az Maliyet</h5>
                <p>Faturalarınızı elektronik ortamda hızlıca oluşturup yıllarca saklayabilirsiniz. Kağıt, kartuş, toner,
                  dosya gibi masraflardan kurtulursunuz. Maliyetten tasarruf edersiniz.</p>
              </div>
              <div className="advantage-item">
                <i className="operation"></i>
                <h5>Hızlı ve Risksiz Operasyon</h5>
                <p>E-mail yoluyla gönderilen faturaların kargoda kaybolma, hasar görme riski yoktur ve karşı tarafa anında ulaşır.
                  Bu sayede tahsilat ve ödeme süreçleriniz kısalır, nakit akışınız hızlanır. Zamandan tasarruf edersiniz.</p>
              </div>
              <div className="advantage-item">
                <i className="archive"></i>
                <h5>Kolay Arşivleme</h5>
                <p>Geleneksel dosyalama ve arşivleme yöntemleriyle uğraşmanıza gerek kalmaz. Faturalarınızı dijital ortamda
                  10 yıl boyunca saklayıp tek tıkla hepsine ulaşabilirsiniz. Yerden tasarruf edersiniz.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 how--solutions-wrapper">
        <div className="container-custom">
          <div className="how--solutions-wrap">
            <h4 className="col-12 invoice-header">eFaturayı Kullanmaya Nasıl Başlarım?</h4>
            <div className="col-12 property--card-wrapper">
              <ProductPropertyCard icon="invoice" head="Hatasız Fatura Hazırlama" text="Hızlı, kolay ve ücretsiz faturalayın!" />
              <ProductPropertyCard icon="current" head="Avantajlı Fiyatlar" text="Cari hesaplarınızı her yerden takip edin." />
              <ProductPropertyCard icon="free" head="Ücretsiz Kurulum" text="Tahsilat ve ödeme durumunuzu izleyin." />
              <ProductPropertyCard icon="cursor" head="Tek Tık Kolaylığı" text="Nakit akışı ve karlılığınızı görerek büyüyün." />
              <ProductPropertyCard icon="happy" head="Müşteri Memnuniyeti" text="Kullanımı kolay e-Fatura hizmeti. Üstelik Ücretsiz!" />
              <ProductPropertyCard icon="einvoice" head="E-Fatura Desteği" text="Başka bir yere gitmeden Jetofis üzerinden digital faturalarınız iptal edin." />

            </div>

          </div>
        </div>
      </div>
      <div className="col-12 how--starter-wrapper">
        <div className="container-custom">
          <div className="how--starter-wrap">
            <h4 className="col-12 invoice-header">eFaturayı Kullanmaya Nasıl Başlarım?</h4>
            <div className="starter--item-wrap">
              <div className="starter-item">
                <i className="licence"></i>
                <p>Lisans Ekibimizle Görüşerek
                  Sözleşme Sürecini Tamamlayın</p>
              </div>
              <div className="starter-item">
                <i className="management"></i>
                <p>Yönetim Panelinde Tek Tuşla
                  Siparişinizi Faturaya Dönüştürün.</p>
              </div>
              <div className="starter-item">
                <i className="call-center"></i>
                <p>Siz Rahat Edin
                  Çağrı Merkeziniz Rahatlasın.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 credit--wrapper">
        <div className="container-custom">
          <div className="col-12 credit--wrap">
            {
              credits.map((c,key) =>(
                <div className="credit--item" key={key}>
                  <div className="credit-name">
                    <p className="name">{c.name}</p>
                    <div className="starter-price">Kurulum Ücreti Yok</div>
                  </div>
                  <div className="credit-property">
                    <div className="credit">{c.credit} Kontör</div>
                    <div className="per-credit">{c.per_credit} TL / Adet</div>
                    <div className="price">{c.price} TL + KDV</div>
                    <div className="btn-primary credit-button">Satın Al</div>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </div>

      <Faq />
      <Brand />
    </>
  )

}

export default Invoice