import Header from "./Header/Header";
import Footer from "./Footer";

function Layout({children}){
    return(
        <>
            <div className="jetofis--wrapper">
                <Header />
                <main className="jetofis--main-wrapper">
                    {children}
                </main>
                <Footer />
            </div>
        </>
    )
}
export default Layout