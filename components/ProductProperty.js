import ProductPropertyCard from "./ProductPropertyCard";

function ProductProperty(){
  return(
    <>
      <div className="col-12 product--property-wrapper">
        <div className="container-custom">
          <div className="col-12 product--property-wrap">
            <i className="jetofis-single-icon"></i>
            <h4 className="head-text">Kolay ve online ön muhasebe programı</h4>
            <p className="desc-text">JetOfis ile cari hesaplarınızı, faturalarınızı, banka ve nakit hesaplarınızı takip edebilirsiniz.
              Birçok özelliği ile işiniz daha da kolaylaşacaktır.</p>
            <div className="col-12 property--card-wrapper">
              <ProductPropertyCard icon="invoice" head="Fatura Takibi" text="Hızlı, kolay ve ücretsiz faturalayın!" />
              <ProductPropertyCard icon="current" head="Cari Hesap Takibi" text="Cari hesaplarınızı her yerden takip edin." />
              <ProductPropertyCard icon="income" head="Gelir Gider Takibi" text="Tahsilat ve ödeme durumunuzu izleyin." />
              <ProductPropertyCard icon="report" head="Raporlama" text="Nakit akışı ve karlılığınızı görerek büyüyün." />
              <ProductPropertyCard icon="e-invoice" head="e-Arşiv,  e-Fatura ve e-İrsaliye" text="Kullanımı kolay e-Fatura hizmeti. Üstelik Ücretsiz!" />
              <ProductPropertyCard icon="cancel" head="e-İptal" text="Başka bir yere gitmeden Jetofis üzerinden digital faturalarınız iptal edin." />
              <ProductPropertyCard icon="outgoing" head="Gelen e-Fatura" text="Başka bir yere gitmeden Jetofis üzerinden digital faturalarınızı Jetofis içerisine aktarın." />
              <ProductPropertyCard icon="online" head="Online Tahsilat" text="e-tahsilat entegrasyonu ile tahsilat süreçlerinizi kolaylaştırın." />
              <ProductPropertyCard icon="integration" head="e-Ticaret Entegrasyonu" text="T-Soft üzerinde oluşan tüm faturalarınızı anında JetOfiste görün." />
              <ProductPropertyCard icon="variant" head="Varyant Özelliği" text="E-ticaretinizle muhasebeniz arasındaki farklılıkları kaldırın." />
              <ProductPropertyCard icon="price" head="Ek Ücretler" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod." />
              <ProductPropertyCard icon="place" head="Pazaryeri Çözümü" text="Pazaryeri komisyonlarıyla ya da borç/alacak durumlarınızı rahatlıkla takip edin." />
            </div>
          </div>
        </div>

      </div>
    </>
  )
}
export default ProductProperty