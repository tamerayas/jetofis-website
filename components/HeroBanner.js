import Link from "next/link";
import Image from "next/image";
import Social from "./Social";
import { Swiper, SwiperSlide } from "swiper/react";

const HeroBanner = () => { // Arrow Fn Component
  return(
    <>
      <div className="hero--banner-wrapper">
       <Social />
        <Swiper loop={true} className="hero--banner-wrap">
          <SwiperSlide className="col-12 banner--item hero--bg">
            <div className="container-custom">
              <div className="col-12 col-lg-6 content-wrap">
                <h4 className="head-text">
                  Jetofis ile her zaman her yerde yönetim elinizde.
                </h4>
                <p className="desc-text">
                  Online olarak her yerden jetofis ile telefon,tablet veya bilgisayarınızla ofisinizi yönetin.
                </p>
                <Link href="https://jetofis.com/panel/kayit">
                  <a className="col-3 btn-secondary">
                    Ücretsiz Dene
                  </a>
                </Link>
              </div>
              <div className="col-12 col-lg-6 img-wrap">
                <Image src="/images/hero-banner/management.webp" width={571} height={365}></Image>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide className="col-12 banner--item hero--bg">
            <div className="container-custom">
              <div className="content-wrap">
                <h4 className="head-text">
                  Jetofis ile her zaman her yerde yönetim elinizde.
                </h4>
                <p className="desc-text">
                  Online olarak her yerden jetofis ile telefon,tablet veya bilgisayarınızla ofisinizi yönetin.
                </p>
                <Link href="https://jetofis.com/panel/kayit">
                  <a className="col-3 btn-secondary">
                    Ücretsiz Dene
                  </a>
                </Link>
              </div>
              <div className="img-wrap">
                <Image src="/images/hero-banner/management.webp" width={571} height={365}></Image>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </>

    )
}

export default HeroBanner