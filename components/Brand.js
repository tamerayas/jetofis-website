import brandState from "../data/brand";
import {useState} from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import dynamic from "next/dynamic";



function Brand(){
  let [brand,brandStatus] = useState(brandState)
  return(
    <Swiper loop={true} spaceBetween={10} breakpoints={{
      540: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      1024: {
        slidesPerView: 7,
        spaceBetween: 10,
      },
    }} className="brands--wrapper">
      {
        brand.map((b,key) => (
          <SwiperSlide className="brand--item" key={key}>
            <img src={b.image} alt=""/>
          </SwiperSlide>
        ))
      }
    </Swiper>
  )
}

export default dynamic(() => Promise.resolve(Brand),{ssr:false}) // Dynamic Component (SSR - CSR verileri match edilmediğinden dolayı bu şekilde yapıldı)