import Link from "next/link";
import Image from "next/image";
import {useState} from "react";
function Footer(){

  let [contact,setContact] = useState({mail:"",gdpr:false})

  const handleInputs = (e) =>{
    setContact({...contact,[e.target.name] : e.target.value})
  }

  const handleCheckbox = (e) => {
    setContact({...contact,[e.target.name] : e.target.checked })
  }

  const handleSubmit = async (e) => {
    const contactResponse = await fetch("http://localhost:8888/api/newsletter",{
      method:'POST',
      headers:{
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify(contact)
    })

    const content = await contactResponse.json()

  }

  return(
    <>
      <footer className="footer--wrapper">
        <div className="col-12 footer--content">
          <div className="container-custom">
            <div className="col-12 footer-wrap">

              <div className="col-12 col-sm-6 col-lg-3 footer--menu">
                <h5>Uygulamalar</h5>
                <div className="col-12 footer--menu-wrap">
                  <ul className="col-12 menu--item-wrap">
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">E-Fatura</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/irsaliye" className="col-12">E-İrsaliye</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">Gelen E-Fatura</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">Pazaryeri Çözümü</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">Online Tahsilat</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">E-Ticaret Entegrasyonu</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-lg-3 footer--menu">
                <h5>Destek</h5>
                <div className="col-12 footer--menu-wrap">
                  <ul className="col-12 menu--item-wrap">
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">Kullanım Kılavuzu</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/irsaliye" className="col-12">SSS</Link>
                    </li>
                    <li className="menu--item">
                      <Link href="/invoice" className="col-12">İletişim</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-lg-3 footer--app">
                <h5>Uygulamalar</h5>
                <div className="col-12 footer--menu-wrap">
                  <ul className="col-12 menu--item-wrap">
                    <li className="menu--item">
                      <a href="/sa" className="col-12">
                        <Image src="/images/element/app-store.svg" width={148} height={46}/>
                      </a>
                    </li>
                    <li className="menu--item">
                      <a href="/sa" className="col-12">
                        <Image src="/images/element/google-play.svg" width={148} height={46}/>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-12 col-sm-6 col-lg-3 footer--newsletter">
                <h5>E-Bülten</h5>
                <div className="col-12 footer--newsletter-wrap">
                  <div className="col-12 input-control">
                    <input type="email" className="newsletter-input" key="mail" name="mail" value={contact.mail} onChange={handleInputs} placeholder="E-Mail adresinizi yazınız..."/>
                  </div>
                  <div className="col-12 input-control-checkbox">
                    <label className="checkbox"
                    ><input type="checkbox" key="gdpr" name="gdpr" value={contact.gdpr} onChange={handleCheckbox}/>
                      <p>KVKK - Aydınlatma Metnini Okudum Onaylıyorum.</p>
                      <span className="checkmark"></span>
                    </label>
                  </div>
                  <div className="col-12 button--wrapper">
                    <button className="btn-secondary" onClick={handleSubmit}>Kayıt Ol</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 copyright--wrapper">
          <div className="container-custom">
            <div className="col-12 copyright">
              Tüm hakları saklıdır. ©{new Date().getFullYear()} Jetofis bir TEKROM markasıdır.
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer