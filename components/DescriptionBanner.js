import Link from "next/link";
import banner from "/styles/modules/Banner.module.scss"

function DescriptionBanner (props){
  const {head,desc,desc2,link,src} = props // TODO: desc2 ayrı değil array olarak al
  return(
    <div className={`description--banner-wrapper ${banner.reverse}`}>
      <div className={`container-custom`}>
        <div className={banner.wrap}>
          <div className={`col-12 col-lg-5 ${banner.content}`}>
            {head && <h4 className={banner.head}>{head}</h4>}
            <p className={banner.desc}>{desc}</p>
            {desc2 && <p className={banner.desc}>{desc2}</p>}
            {link && <Link href={link}>
              <a className="col-3 btn-primary">Detaylar</a>
            </Link>}
          </div>
          <div className={`col-12 col-lg-6 ${banner.image}`}>
            <img src={src} alt=""/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DescriptionBanner