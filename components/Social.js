import Link from "next/link";

function Social(){
  return(
    <div className="social--wrap">
      <Link href="https://www.facebook.com/jetofis/">
        <div className="icon-wrap">
          <i className="facebook-icon"></i>
        </div>
      </Link>
      <Link href="https://www.instagram.com/jetofis/">
        <div className="icon-wrap">
          <i className="instagram-icon"></i>
        </div>
      </Link>
      <Link href="https://tr.linkedin.com/company/jetofis">
        <div className="icon-wrap">
          <i className="linkedin-icon"></i>
        </div>
      </Link>
    </div>
  )
}
export default Social