import faq from "/data/faq"
import React, {useState} from 'react';
import {motion, AnimatePresence} from "framer-motion";
import elementSytle from "/styles/modules/Element.module.scss"
import PropTypes from 'prop-types';

const Faq = (props) => {
  let [status,setStatus] = useState(-2)
  const {header} = props
    return (
      <>
        <div className={`col-12 faq--wrapper ${!header ? 'faq-pages' : ''}`}>
          <div className="container-custom">
            <div className="col-12 faq--wrap">
              {!header ? null : <h4 className="faq-header">Sıkça Sorulan Sorular</h4>}
              <div className="faq--item-wrap">
                {
                  faq.map((f,key) => (
                    <div className={`faq--item ${status == key ? 'active' : ''} ${status == key+1 ? 'active-before' : ''}`}  onClick={() => {setStatus(key)}} key={key}>
                      <div className={`faq--item-head ${status == key ? 'active' : ''}`}>
                        <div className="header--sub-wrap">
                          <span className="order-no">{key+1}</span>
                          <h6>{ f.header }</h6>
                        </div>
                        <i className={`arrow ${status == key ? 'up' : ''} ${elementSytle.arrow}`}></i>
                      </div>
                      <AnimatePresence>
                        {status == key &&
                          <div className="col-12 faq--item-content">
                            <motion.p
                              initial={{ height: 0, opacity: 0 }}
                              animate={{ height: "auto", opacity: 1 }}
                              exit={{ height: 0, opacity: 0 }}
                                      className="content--wrapper">
                              {f.content}
                            </motion.p>
                          </div>
                        }
                      </AnimatePresence>
                    </div>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </>

    )
}

Faq.propTypes = {
  header: PropTypes.bool
}

Faq.defaultProps = {
  header:true
}

export default Faq



