import Image from "next/image";
import {useState} from "react";
function TryIt(){
  let [contact,setContact] = useState({mail:""})

  const handleInputs = (e) =>{
    setContact({...contact,[e.target.name] : e.target.value})
  }

  const handleSubmit = async (e) => {
    const contactResponse = await fetch("http://localhost:8888/api/try",{
      method:'POST',
      headers:{
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify(contact)
    })

    const content = await contactResponse.json()

  }

  return(
    <>
      <div className="col-12 try--it-wrapper">
        <div className="container-custom">
          <div className="col-12 try--content-wrap">
            <div className="col-12 col-lg-4 try-img">
              <Image src="/images/try-it.webp" width={304} height={560}></Image>
            </div>
            <div className="col-12 col-lg-8 try-content">
              <p>Zamandan Kazanın,</p>
              <h4>İşinizi Büyütmek için Diğer İşlerinize Daha Fazla Zaman Ayırın!</h4>
              <h6>Hemen Denemeye Başlayın!</h6>
              <input type="email" key="mail" name="mail" onChange={handleInputs} value={contact.mail} placeholder="E-mail adresiniz"/>
              <button className="btn-primary" onClick={handleSubmit}>Kayıt ol, <span> Ücretsiz Dene</span></button>
            </div>
          </div>
          <div className="col-12 try--card-wrap">
            <div className="try-item">
              <i className="try-device"></i>
              <p>Karmaşık programlar için Mobil uyumluluğu </p>
            </div>
            <div className="try-item">
              <i className="try-time"></i>
              <p> Tüm verileriniz düzenli olarak yedeklenir.</p>
            </div>
            <div className="try-item">
              <i className="try-easy"></i>
              <p>Kullanımı kolay ve oldukça pratiktir. Kurulum gerekmez.</p>
            </div>
            <div className="try-item">
              <i className="try-upload"></i>
              <p>Bilgilere istediğiniz an çevrimiçi olarak da erişebilirsiniz. </p>
            </div>
          </div>
        </div>

      </div>
      <style jsx>{`
        .container-custom{
          display:flex;
          flex-direction:column;
          row-gap:clamp(20px,4.5vw,85px);
          max-width:800px;
        }
      `}</style>
    </>
  )
}
export default TryIt