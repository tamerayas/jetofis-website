import Social from "./Social";
import styles from "/styles/modules/PageHeader.module.scss"
const PageHeader = (props) => {
  const {head,desc} = props
  return(
    <div className={styles.wrap}>
      <Social />
      <div className={`col-12 ${styles.subwrap}`}>
        <div className="container-custom">
          <div className={`col-12 ${styles.content}`}>
            <h4 className="head-text">
              {head}
            </h4>
            <p className="desc-text">
              {desc}
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
export default PageHeader