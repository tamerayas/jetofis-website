import Link from "next/link";
import styles from "/styles/modules/Breadcrumb.module.scss"
const BreadCrumb = (props) => {
  let {breadcrumb} = props
  return(
    <div className="col-12 breadcrumb--wrapper">
      <div className="container-custom">
        <div className={styles.wrap}>
          <ul>
            <li className={styles.item}>
              <Link href="/">
                <a>Anasayfa</a>
              </Link>
            </li>
            {breadcrumb.map((b,key)=>(
              <li className={styles.item} key={key}>
                <Link href={b.href}>
                  <a>{b.name}</a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default BreadCrumb