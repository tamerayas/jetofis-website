import Link from "next/link";
import React from "react";

const subMenu = (props) => {
  const {submenu} = props
  return(
    <>
      <ul className="col-12 sub-menu">
        {submenu.map((s,key) => (
          <li className="sub-menu-item" key={key}>
            <Link href={s.href} >
              <a>{s.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </>
  )

}

export default subMenu