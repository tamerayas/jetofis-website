import Image from "next/image";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import SubMenu from "./SubMenu";
function Header(){
  // eslint-disable-next-line react/display-name
  const Logo = React.forwardRef(({ onClick, href }, ref) => {
    return (
      <a href={href} onClick={onClick} ref={ref}>
        <img src={"/images/logo-jetofis.svg"} width={170} height={62}></img>
      </a>
    )
  })

  let [backToTop,setBackToTop] = useState(false)
  let [subMenu,setSubMenu] = useState(null)

  const handleMenuEnter = (name) =>{
    setSubMenu(name)
  }

  const handleMenuLeave = () =>{
    setSubMenu(null)
  }

  const handleBackToTop = (e) =>{
    document.querySelector('html, body').scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  useEffect(()=>{

    window.addEventListener('scroll',(e)=>{
      const header = document.querySelector('header')
      if (window.scrollY > 0){
        header.classList.add('sticky')
        return
      }
      header.classList.remove('sticky')
    })

    window.addEventListener('scroll', () => {
      const scrolled = window.scrollY

      if (scrolled > 600) {
       return  setBackToTop(true)
      }
      return setBackToTop(false)
    })

    window.addEventListener('resize', () => {
      if (window.innerWidth > 992) {
        mobileMenu.classList.remove('active')
      }

      if (mobileMenu.classList.contains('active')) {
        document.body.style.overflow = 'hidden'
      } else {
        document.body.removeAttribute('style')
      }
    })


    const mobileMenu = document.querySelector('.mobile--menu')

    document
      .querySelector('.hamburger--menu')
      .addEventListener('click', () => {
        mobileMenu.classList.toggle('active')
        document.body.style.overflow = 'hidden'

      })

    document.querySelector('.close--area').addEventListener('click', () => {
      mobileMenu.classList.toggle('active')
      document.body.removeAttribute('style')
    })

    document.querySelectorAll('.mobile--menu a:not(.parent--menu)').forEach(item => {
      item.addEventListener('click', () => {
        mobileMenu.classList.toggle('active')
        document.body.removeAttribute('style')
      })
    })


    const menuToggle = document.querySelectorAll('.parent--menu')

    menuToggle.forEach((element) => {
      element.addEventListener('click', function (e) {
        e.preventDefault()
        console.log('click' , e.target)

        const isActive = element.parentNode.classList.contains('active')
        console.log(element.parentNode.classList.contains('active'))

        if (isActive) {
          element.nextElementSibling.style.height = '0px'
          element.parentNode.classList.remove('active')
          return
        }

        element.nextElementSibling.style.height =
          element.nextElementSibling.scrollHeight + 'px'
        element.parentNode.classList.add('active')
      })
    })

  },[])


  return(
    <>
      <header className="col-12 header--wrapper">
        <div className="container-custom">
          <div className="header--wrap">
            <div className="hamburger--menu"></div>
            <div className="logo-wrap">
              <Link href="/" passHref>
                <Logo />
              </Link>
            </div>
            <nav className="nav--wrapper">
              <ul>
                  <li onMouseEnter={() => handleMenuEnter("özellikler")} onMouseLeave={handleMenuLeave}>
                  <Link href="/property" >
                    <a>Özellikler</a>
                  </Link>
                  {subMenu == "özellikler" && <SubMenu submenu={[
                    {name:"E-Fatura",href:"/invoice"},
                    {name:"E-İrsaliye",href:"/invoice"},
                    {name:"E-Gelen E-Fatura",href:"/invoice"},
                    {name:"Online Tahsilat",href:"/invoice"}
                  ]}/>}
                </li>
                <li>
                  <Link href="/faq" >
                    <a>Destek</a>
                  </Link>
                </li>
                <li onMouseEnter={() => handleMenuEnter("prices")} onMouseLeave={handleMenuLeave}>
                  <Link href="/pricing" >
                    <a>Fiyatlandırma</a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact" >
                    <a>İletişim</a>
                  </Link>
                </li>
              </ul>
            </nav>

            <div className="col-7 menu--wrapper mobile--menu">
              <div className="col-12 menu--mobile-properties">
                <div className="col-12 close--area">
                  <div className="close--text">
                    <i></i>Menü
                  </div>
                  <div className="close--icon"></div>
                </div>
              </div>
              <div className="col-12 mobile--menu-inner">

                <div className="menu--wrap-mobile">
                  <a className="parent--menu">
                    Özellikler
                  </a>
                  <div className="parent--menu-wrap nav--wrapper-mobile">
                    <a href="">E-Fatura</a>
                    <a href="">E-İrsaliye</a>
                    <a href="">E-Fatura Gelen</a>
                    <a href="">Online Tahsilat</a>
                  </div>
                </div>
                <div className="menu--wrap-mobile">
                  <Link href="/faq">Destek</Link>
                </div>
                <div className="menu--wrap-mobile">
                  <Link href="/faq">Fiyatlandırma</Link>
                </div>
                <div className="menu--wrap-mobile">
                  <Link href="/faq">İletişim</Link>
                </div>
                <div className="col-12 menu--properties">
                  <div className="social--mobile-wrap">
                    <div className="header">Bizi Takip Edin</div>
                    <div className="social--item-wrapper">
                      <a rel="noreferrer" target="_blank" className="facebook" href="https://www.facebook.com/jetofis/">
                        <i className="facebook-icon"></i>
                      </a>
                      <a rel="noreferrer" target="_blank" className="instagram" href="https://www.instagram.com/jetofis/">
                        <i className="instagram-icon"></i>
                      </a>
                      <a rel="noreferrer" target="_blank" className="linkedin" href="https://tr.linkedin.com/company/jetofis">
                        <i className="linkedin-icon"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4 col-lg-3 button--wrapper">
              <Link href="https://jetofis.com/panel/giris">
                <div className="btn-primary">
                  Giriş Yap
                </div>
              </Link>
              <Link href="https://jetofis.com/panel/kayit">
                <div className="btn-secondary">
                  Üye Ol
                </div>
              </Link>
            </div>
          </div>
        </div>
      </header>
      {backToTop && <div className="back--to-top" onClick={handleBackToTop}></div>}
    </>
  )
}

export default Header