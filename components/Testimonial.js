import testimionalAr from "../data/testimional";
import {useEffect, useRef, useState} from "react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import {Navigation} from "swiper";
import dynamic from "next/dynamic";

function Testimonial(){
  const leftArrow = useRef(null)
  const rightArrow = useRef(null)
  let [testimional,setTestimional] = useState(testimionalAr)


  return(
    <>
      <div className="col-12 testimional--wrapper">
        <div className="testimional--header">
          <h4>Bizi Tercih Edenler</h4>
          <div className="swiper-navigate-arrow">
            <div className="prev" ref={leftArrow}></div>
            <div className="next" ref={rightArrow}></div>
          </div>
        </div>
        <Swiper loop={true} modules={[Navigation]}
                onInit={(swiper) => {
                  swiper.params.navigation.prevEl = leftArrow.current;
                  swiper.params.navigation.nextEl = rightArrow.current;
                  swiper.navigation.init();
                  swiper.navigation.update();
                }}
                breakpoints={{
                  540: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                  },
                  768:{
                    slidesPerView:2,
                    spaceBetween:10
                  },
                  1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                  }
                }}
                className="testimional--item-wrapper">
          {testimional.map((test,key) =>(
            <SwiperSlide className="col-4 testimional--item" key={key}>
              <div className="col-12 testimional-text">{test.text}</div>
              <div className="col-12 testimional-detail">
                <div className="image-wrap">
                  <Image src={test.image} width={70} height={70}/>
                </div>
                <div className="testimional-info">
                  <div className="testimional-name">{test.name}</div>
                  <div className="testimional-company">{test.company}</div>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </>
  )
}

export default dynamic(()=> Promise.resolve(Testimonial),{ssr:false})