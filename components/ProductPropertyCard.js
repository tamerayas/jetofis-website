function ProductPropertyCard (props) {
  let {icon,head,text} = props;

  return(
    <>
      <div className="property--card-item">
        <i className={`property--card-icon ${icon}`}></i>
        <h6 className="head-text">{head}</h6>
        <p className="desc-text">{text}</p>
      </div>
    </>
  )
}

export default ProductPropertyCard