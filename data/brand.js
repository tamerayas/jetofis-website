let brand = [
  {
    name:"Tekrom",
    image:"/images/brands/tekrom.svg"
  },
  {
    name:"Kütahya Porselen",
    image:"/images/brands/kutahya.svg"
  },
  {
    name:"Fakir",
    image:"/images/brands/fakir.svg"
  },
  {
    name:"Arnica",
    image:"/images/brands/arnica.svg"
  },
  {
    name:"Sunny",
    image:"/images/brands/sunny.svg"
  },
  {
    name:"jump",
    image:"/images/brands/jump.svg"
  }
]

export default brand