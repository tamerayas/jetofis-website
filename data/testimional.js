let testimional = [
  {
    text:"Firmamın işlerini her yerden kolaylıkla takip edebiliyor ve yönetebiliyorum. Bu tam da aradığım şeydi. Teşekkürler…",
    name:"Osman Dere",
    company:"Tekrom A.Ş",
    image:"/images/testimional/1.webp"
  },
  {
    text:"Firmamın işlerini her yerden kolaylıkla takip edebiliyor ve yönetebiliyorum. Bu tam da aradığım şeydi. Teşekkürler…",
    name:"Osman Göl",
    company:"Tsoft A.Ş",
    image:"/images/testimional/2.webp"
  },
  {
    text:"Firmamın işlerini her yerden kolaylıkla takip edebiliyor ve yönetebiliyorum. Bu tam da aradığım şeydi. Teşekkürler…",
    name:"Osman Akarsu",
    company:"Helorobo A.Ş",
    image:"/images/testimional/3.webp"
  }
]

export default testimional