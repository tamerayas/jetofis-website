let property = [
  {
    header:"E-Fatura",
    desc:"e-Fatura, kağıt faturanın elektronik ortamda oluşturulmuş halidir. Kağıt fatura ile bire bir aynı hukuki geçerliliğe sahip olan e-fatura, dijitalleşen dünyada firmaların işlerini kolaylaştırır ve hizmet kalitesini artırır.",
    link:"/invoice",
    image:"/images/property/invoice.webp"
  },
  {
    header:"E-İrsaliye",
    desc:"Kullanımı kolay e-Fatura hizmeti. Üstelik Ücretsiz!",
    image: "/images/property/waybill.webp"
  },
  {
    header:"Gelen E-İrsaliye",
    desc:"Başka bir yere gitmeden Jetofis üzerinden digital faturalarınızı Jetofis içerisine aktarın.",
    image: "/images/property/incoming.webp"
  },
  {
    header:"Pazaryeri Çözümü",
    desc:"Pazaryeri komisyonlarıyla ya da borç/alacak durumlarınızı rahatlıkla takip edin.",
    image: "/images/property/marketplace.webp"
  },
  {
    header:"Online Tahsilat",
    desc:"e-tahsilat entegrasyonu ile tahsilat süreçlerinizi kolaylaştırın.",
    image: "/images/property/online.webp"
  },
  {
    header:"E-Ticaret Entegrasyonu",
    desc:"T-Soft üzerinde oluşan tüm faturalarınızı anında JetOfiste görün.",
    image: "/images/property/integration.webp"
  }
]

export default property